package com.example.task04;

import java.io.IOException;
import java.util.Scanner;

import lombok.Cleanup;

public class Task04Main {
  public static void main(String[] args) throws IOException {
    var scanner = new Scanner(System.in);
    scanner.useDelimiter("\\s");

    double sum = 0;
    while (scanner.hasNext()) {
      String token = scanner.next();
      try {
        sum += Double.parseDouble(token);
      } catch (NumberFormatException e) {}
    }

    System.out.printf("%.6f", sum);
  }
}
