package com.example.task02;

import java.io.IOException;

public class Task02Main {
  public static void main(String[] args) throws IOException {
    int prev = 0;

    for (int ch; (ch = System.in.read()) != -1; prev = ch) {
      switch (ch) {
      case '\n':
        System.out.write('\n');
        break;
      default:
        if (prev == '\r') {
          System.out.write(prev);
        }
        if (ch != '\r') {
          System.out.write(ch);
        }
        break;
      }
    }

    if (prev == '\r') {
      System.out.write(prev);
    }

    System.out.flush();
  }
}
